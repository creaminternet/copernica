# Magento Copernica module #

This is the public GIT repository of the Copernica integration for Magento. 

### Contribution guidelines ###

The master branch is for the currently available version of the module. Development should be done in de develop branch, which contains the latest unreleased code. 

### Who do I talk to? ###

More information about this module en the documentation can be found at the following location: https://docs.cream.nl/display/COPE/Home