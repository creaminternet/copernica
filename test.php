<?php
require_once 'app/Mage.php';

//Varien_Profiler::enable();
//Mage::setIsDeveloperMode(true);
ini_set('display_errors', 1);

umask(0);
Mage::app();

print 'Starttijd:'. time();
print '<hr>';
$obj = Mage::getModel('marketingsoftware/observer');
$obj->processQueue();

print 'Eindtijd:'. time();
print '<hr>';