#!/bin/bash

./vendor/bin/phpcs --config-set installed_paths vendor/magento/marketplace-eqp
./vendor/bin/phpcs --config-set ignore_warnings_on_exit 1
./vendor/bin/phpcs --config-set colors 1
./vendor/bin/phpcs --standard=MEQP1 app/code/community/Copernica/MarketingSoftware
